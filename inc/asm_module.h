/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

/*=====[Avoid multiple inclusion - begin]====================================*/

#ifndef __EJERCICIO_2_ASM_MODULE_H__
#define __EJERCICIO_2_ASM_MODULE_H__

/*=====[Inclusions of public function dependencies]==========================*/

#include "ejercicio_2.h"

/*=====[C++ - begin]=========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/

/*=====[Public function-like macros]=========================================*/

/*=====[Definitions of public data types]====================================*/

/*=====[Prototypes (declarations) of public functions]=======================*/

/*=====[Prototypes (declarations) of public interrupt functions]=============*/

/*=====[External data declaration]===========================================*/

/*=====[external functions declaration]======================================*/

/**
 * @brief C prototype function link with asm_module
 *
 * Automatically when you call and return from function, MCU utilizes these register:
 * 
 * |     C		|  CORE  |
 * | --------- 	| ------ |
 * |  vectorIn	|  r0 	 |
 * |  vectorOut	|  r1  	 |
 * |  longitud	|  r2    |
 * |  escalar 	|  r3    |
 * @param * vectorIn cada elemto se multiplica x escalar
 * @param * vectorOut vector de salida
 * @param longitud
 * @param escalar factor de multiplicacion
 */
extern void asm_productoEscalar32 (volatile uint32_t * vectorIn, volatile uint32_t * vectorOut, uint32_t longitud, uint32_t escalar);

/*=====[C++ - end]===========================================================*/

#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* __EJERCICIO_1_ASM_MODULE__ */
