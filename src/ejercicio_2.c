/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

/*=====[Inclusions of function dependencies]=================================*/

#include "ejercicio_2.h"

#include "asm_module.h"
#include "c_function.h"
#include "sapi.h"

/*=====[Definition macros of private constants]==============================*/
#define elementsof(x)  (sizeof(x) / sizeof((x)[0]))

/*=====[Definitions of extern global variables]==============================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/

/*=====[Main function, program entry point after power on or reset]==========*/

int main(void) {

	/*=====[Definitions of private local variables]=============================*/
	volatile uint32_t asm_vector[]={ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
			12, 13, 14, 15, 16, 17,	18, 19, 20, 21, 22, 23, 24, 25,
			26 };
	volatile uint32_t c_vector[]={ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
			12, 13, 14, 15, 16, 17,	18, 19, 20, 21, 22, 23, 24, 25,
			26 };
	volatile uint32_t asm_vectorOut[elementsof(asm_vector)];
	uint32_t c_vectorOut[elementsof(c_vector)];
	uint32_t cyclesElapsed = 0;

// ----- Setup -----------------------------------
	boardInit();

   // Configura el contador de ciclos con el clock de la EDU-CIAA NXP
   cyclesCounterConfig(EDU_CIAA_NXP_CLOCK_SPEED);

   printf("asm_vector = {");
   for (int var = 0; var < elementsof(asm_vector) - 2; var++) {
	   printf("%d, ", asm_vector[var]);
   }
   printf("%d }\r\n", asm_vector[elementsof(asm_vector) - 1]);

   // Resetea el contador de ciclos
   cyclesCounterReset();
   //Ejecuta funcion asm
   asm_productoEscalar32(asm_vector, asm_vectorOut, elementsof(asm_vector), 4);

   // Guarda en una variable los ciclos leidos
   cyclesElapsed = cyclesCounterRead();
   printf( "La funcion asm se ejecuto en %d ciclos\r\n", cyclesElapsed );

   printf("El resultado fue asm_vector = {");
   for (int var = 0; var < elementsof(asm_vectorOut) - 2; var++) {
	   printf("%d, ", asm_vectorOut[var]);
   }
   printf("%d }\r\n", asm_vectorOut[elementsof(asm_vectorOut) - 1]);

   printf("c_vector = {");
   for (int var = 0; var < elementsof(c_vector) - 2; var++) {
	   printf("%d, ", c_vector[var]);
   }
   printf("%d }\r\n", c_vector[elementsof(c_vector) - 1]);

   // Resetea el contador de ciclos
   cyclesCounterReset();
   //Ejecuta funcion C
   c_productoEscalar32(c_vector, c_vectorOut, elementsof(c_vector), 4);

   // Guarda en una variable los ciclos leidos
   cyclesElapsed = cyclesCounterRead();
   printf( "La funcion C se ejecuto en %d ciclos\r\n", cyclesElapsed );

   printf("El resultado fue c_vector = {");
   for (int var = 0; var < elementsof(c_vectorOut) - 2; var++) {
	   printf("%d, ", c_vectorOut[var]);
   }
   printf("%d }\r\n", c_vectorOut[elementsof(c_vectorOut) - 1]);

// ----- Repeat for ever -------------------------
	while ( true) {
		__WFI(); //wfi
	}

	// YOU NEVER REACH HERE, because this program runs directly or on a
	// microcontroller and is not called by any Operating System, as in the
	// case of a PC program.
	return 0;
}
