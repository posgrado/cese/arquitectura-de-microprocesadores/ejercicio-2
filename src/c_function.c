/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

#include "c_function.h"

void c_productoEscalar32 (volatile uint32_t * vectorIn, uint32_t * vectorOut, uint32_t longitud, uint32_t escalar){
	for (uint32_t var = 0; var < longitud; ++var) {
		vectorOut[var] = vectorIn[var] * escalar;
	}
}

